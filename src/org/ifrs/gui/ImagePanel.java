package org.ifrs.gui;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author Helder
 */
public class ImagePanel extends JPanel{
    
    private BufferedImage image;
    
    public ImagePanel(){
        this.setSize(800, 640);
        
        try {                
              image = ImageIO.read(new File("src/imgs/header.jpg"));
           } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, "Imagem de cabeçalho não encontrada", "Atenção", 2);
           }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, 0, 0, null); // see javadoc for more info on the parameters            
    }
}
