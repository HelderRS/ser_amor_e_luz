package org.ifrs.gui.custom;

import java.util.ArrayList;

/**
 *
 * @author Helder
 */
public class ViewController {
    
    private static ArrayList<CustomForm> listView = new ArrayList<CustomForm>();
    private static ViewController viewController;
    public static final String MODULES_FORM = "modules";
    public static final String CLIENTES_FORM = "clientes";
    public static final String LOGIN_FORM = "login";
    public static final String NUMEROLOGIA_FORM = "numerologia";
    public static final String CADASTROS_FORM = "cadastros";
    public static final String PESQUISA_CLIENTES_FORM = "pesquisaClientes";
    
    private ViewController(){
        
    }
    
    public static ViewController getInstance(){
        if(viewController == null){
            viewController = new ViewController();
        }
        return viewController;
    }
    
    public static void addView(CustomForm form){
        listView.add(form);
    }
    
    public static CustomForm getView(String formName){
        for(CustomForm form : listView){
            if(form.formName == formName){
                return form;
            }
        }
        return null;
    }
    
}
