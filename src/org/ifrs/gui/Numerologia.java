package org.ifrs.gui;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.ifrs.controller.NumerologiaController;
import org.ifrs.gui.custom.CustomForm;
import org.ifrs.gui.custom.CustomFormJPanel;
import org.ifrs.gui.custom.CustomPopUp;
import org.ifrs.gui.custom.ViewController;
import org.ifrs.model.Cliente;

/**
 *
 * @author Helder
 */
public class Numerologia extends CustomFormJPanel {

    Cliente cliente;
    
    /**
     * Creates new form Numerologia
     */
    public Numerologia() {
        super(ViewController.getView(ViewController.MODULES_FORM));
        initComponents();
        this.setCabecalho("Numerologia");
        
//        btnPessoalVerTela.setIcon(new ImageIcon(getClass().getResource("/imgs/search.png")));  
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtPessoal = new javax.swing.JLabel();
        btnPessoalVisualizar = new org.ifrs.gui.custom.CustomJButton();
        btnPessoalImprimir = new org.ifrs.gui.custom.CustomJButton();
        btnPessoalGerarPdf = new org.ifrs.gui.custom.CustomJButton();
        btnPessoalSimplificado = new org.ifrs.gui.custom.CustomJButton();
        btnPessoalEnviaEmail = new org.ifrs.gui.custom.CustomJButton();
        btnPesquisar = new org.ifrs.gui.custom.CustomJButton();
        jLabel1 = new javax.swing.JLabel();
        txtNome = new javax.swing.JTextField();

        setMaximumSize(new java.awt.Dimension(800, 600));
        setMinimumSize(new java.awt.Dimension(800, 600));
        setPreferredSize(new java.awt.Dimension(800, 600));

        txtPessoal.setText("Pessoal");

        btnPessoalVisualizar.setText("Visualizar em Tela");
        btnPessoalVisualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPessoalVisualizarActionPerformed(evt);
            }
        });

        btnPessoalImprimir.setText("Imprimir");

        btnPessoalGerarPdf.setText("Gerar PDF");

        btnPessoalSimplificado.setText("Simplificado");
        btnPessoalSimplificado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPessoalSimplificadoActionPerformed(evt);
            }
        });

        btnPessoalEnviaEmail.setText("Envia E-mail");

        btnPesquisar.setText("Pesquisar");
        btnPesquisar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnPesquisarMouseClicked(evt);
            }
        });

        jLabel1.setText("Cliente:");

        txtNome.setEditable(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(133, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(txtPessoal)
                        .addGap(24, 24, 24)
                        .addComponent(btnPessoalVisualizar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(14, 14, 14)
                        .addComponent(btnPessoalImprimir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(btnPessoalGerarPdf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(9, 9, 9)
                        .addComponent(btnPessoalSimplificado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(13, 13, 13))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, 329, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPessoalEnviaEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(118, 118, 118))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(255, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(73, 73, 73)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtPessoal)
                    .addComponent(btnPessoalVisualizar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPessoalImprimir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPessoalGerarPdf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPessoalSimplificado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPessoalEnviaEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(228, 228, 228))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnPesquisarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPesquisarMouseClicked
        CustomPopUp popUp = new CustomPopUp();
        PesquisaClientes pesquisaClientes = new PesquisaClientes(this, ViewController.NUMEROLOGIA_FORM);
        pesquisaClientes.setFather(popUp);
        popUp.add(pesquisaClientes);
        popUp.setVisible(true);
      
        
    }//GEN-LAST:event_btnPesquisarMouseClicked

    private void btnPessoalSimplificadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPessoalSimplificadoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnPessoalSimplificadoActionPerformed

    private void btnPessoalVisualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPessoalVisualizarActionPerformed
        NumerologiaController numControl = new NumerologiaController();
        numControl.prepareDoc(cliente);
        numControl.createNumerologia();
        File file = numControl.finalizeDoc();
        try {
            Desktop.getDesktop().open(file);
        } catch (IOException ex) {
            Logger.getLogger(Numerologia.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnPessoalVisualizarActionPerformed

    public void setCliente(Cliente cliente){
        this.cliente = cliente;
        System.out.println(cliente.getNome());
        this.txtNome.setText(cliente.getNome());
        
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private org.ifrs.gui.custom.CustomJButton btnPesquisar;
    private org.ifrs.gui.custom.CustomJButton btnPessoalEnviaEmail;
    private org.ifrs.gui.custom.CustomJButton btnPessoalGerarPdf;
    private org.ifrs.gui.custom.CustomJButton btnPessoalImprimir;
    private org.ifrs.gui.custom.CustomJButton btnPessoalSimplificado;
    private org.ifrs.gui.custom.CustomJButton btnPessoalVisualizar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JTextField txtNome;
    private javax.swing.JLabel txtPessoal;
    // End of variables declaration//GEN-END:variables
}


