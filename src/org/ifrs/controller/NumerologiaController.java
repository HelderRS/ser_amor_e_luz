package org.ifrs.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.xwpf.usermodel.Borders;
import org.apache.poi.xwpf.usermodel.BreakType;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.TextAlignment;
import org.apache.poi.xwpf.usermodel.UnderlinePatterns;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.ifrs.model.Cliente;

/**
 *
 * @author Helder
 */
public class NumerologiaController {
    private XWPFDocument doc;
    private Cliente cliente;
    
    public void prepareDoc(Cliente cliente){
        this.cliente = cliente;
        this.doc = new XWPFDocument();    
        
        XWPFParagraph p1 = doc.createParagraph();
        p1.setAlignment(ParagraphAlignment.CENTER);
        p1.setVerticalAlignment(TextAlignment.CENTER);

        XWPFRun r1 = p1.createRun();
        r1.setBold(true);
        r1.setText("CAPA");
        r1.setFontSize(50);
        r1.setBold(true);
        r1.setFontFamily("Times New Roman");
        r1.setTextPosition(100);
        r1.addBreak(BreakType.PAGE);
        
    }
    
    public void createNumerologia(){
        XWPFParagraph p1 = doc.createParagraph();
        p1.setAlignment(ParagraphAlignment.CENTER);
        p1.setVerticalAlignment(TextAlignment.CENTER);

        XWPFRun r1 = p1.createRun();
        r1.setFontSize(18);
        r1.setItalic(true);
        r1.setText("Esta é a numerologia de: " + this.cliente.getNome() );
        r1.addBreak(BreakType.PAGE);
        
    }
    
    public File finalizeDoc(){
        FileOutputStream out;
        File file = new File("C:/temp/" + this.cliente.getId_cliente() + 
                    "_" + this.cliente.getNome() + ".docx");
        try {
            out = new FileOutputStream(file.getAbsolutePath());
            doc.write(out);
            out.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(NumerologiaController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(NumerologiaController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return file;
        
    }
}
