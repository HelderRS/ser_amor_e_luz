package org.ifrs.model;

import java.sql.Date;
import java.util.HashMap;

/**
 *
 * @author Helder
 */
public class Cliente {
    
    private Long id_cliente;
    private String nome;
    private Date dataNascimento;
    private String sexo;
    private String email;
    private String telefone;
    private HashMap<String, Integer> vogais;
    private HashMap<String, Integer> consoantes;
    
    public Cliente(){
        this.id_cliente = Long.MIN_VALUE;
    }

    public Long getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(Long id_cliente) {
        this.id_cliente = id_cliente;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
    
    public Cliente clone(){
        Cliente cliente = new Cliente();
        this.id_cliente     = cliente.id_cliente;
        this.nome           = cliente.nome;
        this.dataNascimento = cliente.dataNascimento;
        this.sexo           = cliente.sexo;
        this.email          = cliente.email;
        this.telefone       = cliente.telefone;
        return cliente;
        
    }
    
    public int calcularVogais(){
        int resultado = 0;
        
        if (vogais == null){
            this.populateVogais();
        }
    
        for(int i = 0; i < this.nome.length(); i++){
            if (vogais.get(String.valueOf(nome.charAt(i)).toLowerCase()) != null){
                resultado += vogais.get(String.valueOf(nome.charAt(i)).toLowerCase());
            }
        }
        
        if(resultado > 78){
            resultado = normalizeResultado(resultado);
        }
        
        
        return resultado;
    }
    
    public void populateVogais(){
        vogais = new HashMap<String, Integer>();
        vogais.put("a", 1);
        vogais.put("e", 5);
        vogais.put("i", 9);
        vogais.put("o", 6);
        vogais.put("u", 3);
    }
    
    public int calcularConsoantes(){
        int resultado = 0;
        
        if (consoantes == null){
            this.populateConsoantes();
        }
        
        for(int i = 0; i < this.nome.length(); i++){
            if (consoantes.get(String.valueOf(nome.charAt(i)).toLowerCase()) != null){
                resultado += consoantes.get(String.valueOf(nome.charAt(i)).toLowerCase());
            }
        }    
        
        if(resultado > 78){
            resultado = normalizeResultado(resultado);
        }
                
        return resultado;
    }
    
    public void populateConsoantes(){
        consoantes = new HashMap<String, Integer>();
        consoantes.put("b", 2);
        consoantes.put("c", 3);
        consoantes.put("d", 4);
        consoantes.put("f", 6);
        consoantes.put("g", 7);
        consoantes.put("h", 8);
        consoantes.put("j", 1);
        consoantes.put("k", 2);
        consoantes.put("l", 3);
        consoantes.put("m", 4);
        consoantes.put("n", 5);
        consoantes.put("p", 7);
        consoantes.put("q", 8);
        consoantes.put("r", 9);
        consoantes.put("s", 1);
        consoantes.put("t", 2);
        consoantes.put("v", 4);
        consoantes.put("w", 5);
        consoantes.put("x", 6);
        consoantes.put("y", 7);
        consoantes.put("z", 8);
    }
    
    public int normalizeResultado(int resultado){
        String aux = String.valueOf(resultado);
        resultado = 0;
        for (int i = 0; i < aux.length(); i++){
            resultado += Integer.parseInt(String.valueOf(aux.charAt(i)));
        }   
        return resultado;
    }
    
    public int calcularNome(){
        int resultado = 0;
        if (consoantes == null){
            this.populateConsoantes();
        }
        if (vogais == null){
            this.populateVogais();
        }
        for(int i = 0; i < this.nome.length(); i++){
            if (consoantes.get(String.valueOf(nome.charAt(i)).toLowerCase()) != null){
                resultado += consoantes.get(String.valueOf(nome.charAt(i)).toLowerCase());
            }else if(vogais.get(String.valueOf(nome.charAt(i)).toLowerCase()) != null){
                resultado += vogais.get(String.valueOf(nome.charAt(i)).toLowerCase());
            }
        } 
        if(resultado > 78){
            resultado = normalizeResultado(resultado);
        }
        
        
        
        
        return resultado;
    }
        
    
}
