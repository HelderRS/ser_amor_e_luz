package org.ifrs.dao;

import java.awt.List;
import java.sql.*;
import java.sql.PreparedStatement;
import java.lang.Integer;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import org.ifrs.factory.ConnectionFactory;
import org.ifrs.model.Cliente;

/**
 *
 * @author Helder
 */
public class ClienteDAO {
    private Connection connection;
    private Long id_cliente;
    private String nome;
    private java.util.Date dataNascimento;
    private String sexo;
    private String email;
    private String telefone;
    
    public ClienteDAO(){
        this.connection = ConnectionFactory.getInstance();
        
    }
    
    public void adiciona(Cliente cliente) throws SQLException {
        String sql = "INSERT INTO cliente(nome,data_nasc,sexo,email,telefone) VALUES(?,?,?,?,?)";
        PreparedStatement stmt = null;
        try{
            stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, cliente.getNome());
            stmt.setDate(2, cliente.getDataNascimento());
            stmt.setString(3, cliente.getSexo());
            stmt.setString(4, cliente.getEmail());
            stmt.setString(5, cliente.getTelefone());
            int affectedRows = stmt.executeUpdate();
            
            if (affectedRows == 0) {
                throw new SQLException("Creating user failed, no rows affected.");
            }
            try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    cliente.setId_cliente(generatedKeys.getLong(1));
                }
                else {
                    throw new SQLException("Creating user failed, no ID obtained.");
                }
            }
        }catch(SQLException e){
            throw new RuntimeException(e);
        }finally{
            stmt.close();
        }
    }
    
    public void updateCliente(Cliente cliente) throws SQLException{
        String sql = "update cliente set cliente.nome=?, cliente.data_nasc=?, cliente.sexo=?, cliente.email=?, cliente.telefone=? where cliente.id_cliente = ?;";
        PreparedStatement stmt = null;
        try{
            stmt = connection.prepareStatement(sql);
            stmt.setString(1, cliente.getNome());
            stmt.setDate(2, cliente.getDataNascimento());
            stmt.setString(3, cliente.getSexo());
            stmt.setString(4, cliente.getEmail());
            stmt.setString(5, cliente.getTelefone());
            stmt.setLong(6, cliente.getId_cliente());
            stmt.execute();
            
        }catch(SQLException e){
            throw new RuntimeException(e);
        }finally{
            stmt.close();
        }
    }
    
    public ArrayList<Cliente> getClientes(String nome){
        
        String sql = "select id_cliente, nome, data_nasc, email, telefone from cliente where cliente.nome > \"" + nome + "\"";
        ResultSet rs = null;
        ArrayList<Cliente> listaClientes = new ArrayList<Cliente>();
        
        try{
            
            PreparedStatement stmt = connection.prepareStatement(sql);
                        
            rs = stmt.executeQuery();
            while(rs.next()){
                Cliente cliente = new Cliente();
                cliente.setId_cliente(rs.getLong("id_cliente"));
                cliente.setNome(rs.getString("nome"));
                cliente.setDataNascimento(rs.getDate("data_nasc"));
                cliente.setEmail(rs.getString("email"));
                cliente.setTelefone(rs.getString("telefone"));
                listaClientes.add(cliente);
            }
                        
            stmt.close();
            rs.close();
            
                    
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Não foi possível consultar clientes no banco");
        }
        return listaClientes;
    }
}

