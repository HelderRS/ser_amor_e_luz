package org.ifrs.factory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Helder
 */
public class ConnectionFactory {
    
    private static Connection connection = null;
    
    private static ConnectionFactory connectionFactory = null;
    
    public static Connection getInstance(){
        if (connectionFactory==null) {
            connectionFactory = new ConnectionFactory();
        }
        return connection;
    }
    
    private ConnectionFactory(){
        try{
            this.connection = DriverManager.getConnection("jdbc:mysql://localhost/ser_amor_e_luz", "root", "root");
                        
        }catch(SQLException exception){
            throw new RuntimeException(exception);
        }
    }
    
}
